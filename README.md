# CS GO config

My cs go config files.

## Viewmodel
```viewmodel_fov 65; viewmodel_offset_x 2; viewmodel_offset_y 1.5; viewmodel_offset_z -1; viewmodel_presetpos 0; cl_viewmodel_shift_left_amt 1.5; cl_viewmodel_shift_right_amt 0.75; viewmodel_recoil 0; cl_righthand 1;```

## Crosshairs
| Crosshair Code                              |  Description|
:---------------------------------------------|-------------------
CSGO-hOheq-esS8J-CsJAy-ov4EH-NDjLJ            |  Small Green
CSGO-6puA8-RmGAk-C8Qj7-OanWi-yuN6H            |  Small Blue
CSGO-AckHS-WEuMs-oCB8S-4VRda-6DR7B            |  Large White
CSGO-yarku-Qsxbs-Y9yVE-tRpSV-VjaAB            |  Dynamic Small Blue 
CSGO-MVEcX-dqhby-cLaQu-6dP2W-dQunC            |  Ant0hak
